﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VirtualLibrary.Models;

namespace VirtualLibrary.DAL
{
    public class LibraryInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<LibraryContext>
    {
        protected override void Seed(LibraryContext context)
        {
            var books = new List<Book>
            {
                new Book{Author="Dostojewski, Fiodor", BookID=1, IsBorrowed=true, Publisher="Greg", CheckingOutDate=null, Genre = VirtualLibrary.Models.Book.Genres.Beletrystyka, Title = "Zbrodnia i Kara", UserID = null, Year = 2003},
                new Book{Author="Orwell, George", BookID=2, IsBorrowed=false, Publisher="Muza", CheckingOutDate=null, Genre = Book.Genres.Beletrystyka, Title = "Rok 1984", UserID = null, Year = 2006, Description="Wielki Brat Patrzy – to właśnie napisy tej treści, w antyutopii Orwella krzyczące z plakatów rozlepionych po całym Londynie, natchnęły twórców telewizyjnego show „Big Brother”. Czyżby wraz z upadkiem komunizmu wielka, oskarżycielska powieść straciła swoją rację bytu, stając się zaledwie inspiracją programu rozrywkowego? Nie. Bo ukazuje świat, który zawsze może powrócić. Świat pustych sklepów, permanentnej wojny, jednej wiary. Klaustrofobiczny świat Wielkiego Brata, w którym każda sekunda ludzkiego życia znajduje się pod kontrolą, a dominującym uczuciem jest strach. Świat, w którym ludzie czują się bezradni i samotni, miłość uchodzi za zbrodnię, a takie pojęcie jak ‘wolność” i „sprawiedliwość” nie istnieją. Na kuli ziemskiej są miejsca, gdzie ten świat wciąż trwa. I zawsze znajdą się cudotwórcy gotowi obiecywać stworzenie nowego, który od wizji Orwella dzieli tylko krok. Niestety, piekło wybrukowane jest dobrymi chęciami. A dwa plus dwa wcale nie musi się równać cztery."},
                new Book{Author="Stallings, William", BookID=3, IsBorrowed=false, Publisher="Wydawnictwo Naukowo-Techniczne", CheckingOutDate=null, Genre = Book.Genres.Naukowa, Title="Organizacja i architektura systemu komputerowego", UserID=null, Year=2002}
            };

            books.ForEach(b => context.Books.Add(b));
            context.SaveChanges();
        }
    }
}