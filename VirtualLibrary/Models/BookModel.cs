﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace VirtualLibrary.Models
{
    public class Book
    {
        public enum Genres
        {
            Dramat, 
            Fantasy, 
            Horror, 
            Kryminał, 
            Romans, 
            Biografia,
            Naukowa, 
            Poezja, 
            Przygodowa,
            Beletrystyka, 
            Poradnik, 
            Inna,

            [Display(Name = "Dla Dzieci")]
            DlaDzieci,
            [Display(Name = "Popularno-Naukowa")]
            PopularnoNaukowa
        }

        public int BookID { get; set; }
        
        [Required]
        [Display(Name = "Autor")]
        [StringLength(50, MinimumLength = 1)]
        public string Author { get; set; }
        
        [Required]
        [Display(Name = "Tytuł")]
        [StringLength(50, MinimumLength = 1)]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Wydawnictwo")]
        [StringLength(50, MinimumLength = 1)]
        public string Publisher { get; set; }

        [Required]
        [Display(Name = "Gatunek")]
        public Genres Genre { get; set; }

        [Display(Name = "Rok Wydania")]
        [Range(1000, 9999)]
        public short Year
        { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Data Wypożyczenia")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true, NullDisplayText = "-")]
        public DateTime? CheckingOutDate { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Data Zwrotu")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true, NullDisplayText = "-")]
        public DateTime? ReturnDate { get; set; }

        [Display(Name = "Wypożyczona")]
        public bool IsBorrowed { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Opis")]
        [DisplayFormat(NullDisplayText="brak opisu")]
        public string Description { get; set; }

        [Display(Name = "Adres URL okładki")]
        [DataType(DataType.ImageUrl)]
        public string ImageURL { get; set; }

        public string UserID { get; set; }
    }
}