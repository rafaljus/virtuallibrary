﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using VirtualLibrary.Models;
using VirtualLibrary.DAL;
using Microsoft.AspNet.Identity.EntityFramework;

namespace VirtualLibrary.Controllers
{
    public class BookController : Controller
    {
        private LibraryContext db = new LibraryContext();
       
        // GET: /Book/
        [Authorize]
        public ActionResult Index(string sortOrder, string searchString, string genreType)
        {
            var books = from b in db.Books
                           select b;

            bool isSearchStringEmpty = !String.IsNullOrEmpty(searchString);
            bool isGenreTypeEmpty = !String.IsNullOrEmpty(genreType);

            if (isSearchStringEmpty && isGenreTypeEmpty)
            {
                books = books.Where(b => (b.Author.Contains(searchString) && b.Genre.ToString().Contains(genreType))
                    || (b.Title.Contains(searchString) && b.Genre.ToString().Contains(genreType)));
            }
            else if (isSearchStringEmpty)
            {
                books = books.Where(b => b.Author.Contains(searchString) || b.Title.Contains(searchString));
            }
            else if (isGenreTypeEmpty)
            {
                books = books.Where(b => b.Genre.ToString().Contains(genreType));
            }
            
            switch (sortOrder)
            {
                case "Author":
                    books = books.OrderBy(b => b.Author);
                    break;
                case "Title":
                    books = books.OrderBy(b => b.Title);
                    break;
                case "Genre":
                    books = books.OrderBy(b => b.Genre.ToString());
                    break;
                case "Year":
                    books = books.OrderBy(b => b.Year);
                    break;
                case "Publisher":
                    books = books.OrderBy(b => b.Publisher);
                    break;
                case "IsBorrowed":
                    books = books.OrderBy(b => b.IsBorrowed.ToString());
                    break;
                default:
                    books = books.OrderBy(b => b.BookID);
                    break;
            }
            return View(books.ToList());
        }

        // GET: /Book/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // GET: /Book/Create
        [Authorize(Roles="admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Book/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles="admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind] Book book)
        {
            if (ModelState.IsValid)
            {
                book.IsBorrowed = false;
                db.Books.Add(book);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(book);
        }

        // GET: /Book/Edit/5
        [Authorize(Roles="admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // POST: /Book/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles="admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind] Book book)
        {
            if (ModelState.IsValid)
            {
                db.Entry(book).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(book);
        }

        // GET: /Book/Delete/5
        [Authorize(Roles="admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // POST: /Book/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize(Roles="admin")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Book book = db.Books.Find(id);
            db.Books.Remove(book);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: /Book/CheckOut/5
        [Authorize]
        public ActionResult CheckOut(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // POST: /Book/CheckOut/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Checkout")]
        [ValidateAntiForgeryToken]
        public ActionResult CheckOutConfirmed(int id)
        {
            Book book = db.Books.Find(id);
            if (ModelState.IsValid)
            {
                string userID = (string)User.Identity.GetUserId();

                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new VirtualLibrary.Models.ApplicationDbContext()));
                ApplicationUser user = userManager.FindById<ApplicationUser>(userID);
                user.HasRentedABook = true;

                db.Entry(book).Property("CheckingOutDate").CurrentValue = DateTime.Now;
                db.Entry(book).Property("ReturnDate").CurrentValue = DateTime.Now.AddDays(30);
                db.Entry(book).Property("UserID").CurrentValue = User.Identity.GetUserId();
                db.Entry(book).Property("IsBorrowed").CurrentValue = true;
                db.Entry(book).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else if (!ModelState.IsValid)
            {
                var errors = ModelState.SelectMany(x => x.Value.Errors.Select(z => z.Exception));

                // Breakpoint, Log or examine the list with Exceptions.
            }
            return View(book);
        }

        [ActionName("Return")]
        [Authorize(Roles = "admin")]
        public ActionResult Return(int id)
        {
            Book book = db.Books.Find(id);
            if (ModelState.IsValid)
            {
                string userID = (string)db.Entry(book).Property("UserID").CurrentValue;

                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new VirtualLibrary.Models.ApplicationDbContext()));
                ApplicationUser user = userManager.FindById<ApplicationUser>(userID);
                user.HasRentedABook = false;

                db.Entry(book).Property("IsBorrowed").CurrentValue = false;
                db.Entry(book).Property("ReturnDate").CurrentValue = null;
                db.Entry(book).Property("UserID").CurrentValue = null;
                db.Entry(book).Property("CheckingOutDate").CurrentValue = null;
                db.Entry(book).State = EntityState.Modified;
                db.SaveChanges();

                
            }
            else if (!ModelState.IsValid)
            {
                var errors = ModelState.SelectMany(x => x.Value.Errors.Select(z => z.Exception));

                // Breakpoint, Log or examine the list with Exceptions.
            }
            return View(book);
        }

        // GET: /Book/Details/5
        [Authorize(Roles = "admin")]
        public ActionResult UserDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
